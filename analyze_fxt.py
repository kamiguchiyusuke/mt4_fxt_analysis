from struct import unpack
import datetime

filename = 'GBPJPY1_0.fxt'

#参照
#https://github.com/EA31337/MT-Formats/blob/master/fxt-405-refined.mqh

def holc(reader):
    print("")
    print("opentime :", datetime.datetime.fromtimestamp(unpack("I", reader.read(4))[0]).strftime('%Y-%m-%d %H:%M:%S'))
    unpack("I", reader.read(4))
    olhc = unpack("4d", reader.read(32))
    print("oepn :", olhc[0])
    print("low :", olhc[1])
    print("high :", olhc[2])
    print("close :", olhc[3])
    print("volume : ", unpack("I", reader.read(4))[0])
    unpack("I", reader.read(4))
    print("closetime : ", datetime.datetime.fromtimestamp(unpack("I", reader.read(4))[0]).strftime('%Y-%m-%d %H:%M:%S'))
    unpack("I", reader.read(4))    


with open(filename, 'rb') as f:
    print("version : ", unpack("i", f.read(4))[0])
    print("copyright : ", unpack("64s", f.read(64))[0].strip(b'\x00').decode())
    print("brocker : ", unpack("128s", f.read(128))[0].strip(b'\x00').decode())
    print("symbol : ", unpack("12s", f.read(12))[0].strip(b'\x00').decode())
    print("period : ", unpack("i", f.read(4))[0])
    print("model : ", unpack("i", f.read(4))[0], " // Model type: 0 - every tick, 1 - control points, 2 - bar open.")
    print("bars : ", unpack("i", f.read(4))[0], " // Bars - number of modeled bars in history.")
    print("fromdate : ", datetime.datetime.fromtimestamp(unpack("I", f.read(4))[0]).strftime('%Y-%m-%d %H:%M:%S'))
    print("todate : ", datetime.datetime.fromtimestamp(unpack("I", f.read(4))[0]).strftime('%Y-%m-%d %H:%M:%S'))    
    print("totalTicks : ", unpack("i", f.read(4))[0])
    print("modelquality : ", unpack("d", f.read(8))[0], " // Modeling quality (max. 99.9)")

    print("currency : ", unpack("12s", f.read(12))[0].strip(b'\x00').decode())
    print("spread : ", unpack("i", f.read(4))[0])
    print("digits : ", unpack("i", f.read(4))[0])
    unpack("i", f.read(4))

    print("point : ", unpack("d", f.read(8))[0], " // Point size (e.g. 0.00001). Same as: MarketInfo(MODE_POINT) 通貨ペアのポイント")
    print("lot_min : ", unpack("i", f.read(4))[0], " // minimum lot size")
    print("lot_max : ", unpack("i", f.read(4))[0], " // maximum lot size")
    print("lot_step : ", unpack("i", f.read(4))[0])
    print("stops_level : ", unpack("i", f.read(4))[0], " // stops level value")
    print("gtc_pendings : ", unpack("i", f.read(4))[0], " // instruction to close pending orders at the end of day")
    unpack("i", f.read(4))

    print("contract_size : ", unpack("d", f.read(8))[0], " // contract size")
    print("tick_value : ", unpack("d", f.read(8))[0], " // value of one tick")
    print("tick_size : ", unpack("d", f.read(8))[0], "  // size of one tick")
    print("profit_mode : ", unpack("i", f.read(4))[0], "  // Profit calculation mode { PROFIT_CALC_FOREX=0, PROFIT_CALC_CFD=1, PROFIT_CALC_FUTURES=2 }. Same as: MarketInfo(MODE_PROFITCALCMODE)")

    print("swap_enable : ", unpack("i", f.read(4))[0])
    print("swap_type : ", unpack("i", f.read(4))[0])
    unpack("i", f.read(4))#padding
    print("swap_long : ", unpack("d", f.read(8))[0])
    print("swap_short : ", unpack("d", f.read(8))[0])
    print("swap_rollover3days : ", unpack("i", f.read(4))[0])

    print("leverage : ", unpack("i", f.read(4))[0])
    print("free_margin_mode : ", unpack("i", f.read(4))[0])
    print("margin_mode : ", unpack("i", f.read(4))[0])
    print("margin_stopout : ", unpack("i", f.read(4))[0])

    print("margin_stopout_mode : ", unpack("i", f.read(4))[0])
    print("margin_initial : ", unpack("d", f.read(8))[0])
    print("margin_maintenance : ", unpack("d", f.read(8))[0])
    print("margin_hedged : ", unpack("d", f.read(8))[0])
    print("margin_divider : ", unpack("d", f.read(8))[0])
    print("margin_currency : ", unpack("12s", f.read(12))[0].strip(b'\x00').decode())
    unpack("i", f.read(4))#padding

    print("comm_base : ", unpack("d", f.read(8))[0])
    print("comm_type : ", unpack("i", f.read(4))[0])
    print("comm_lots : ", unpack("i", f.read(4))[0])

    print("from_bar : ", unpack("i", f.read(4))[0])
    print("to_bar : ", unpack("i", f.read(4))[0])
    print("start_period_m1 : ", unpack("i", f.read(4))[0])
    print("start_period_m5 : ", unpack("i", f.read(4))[0])
    print("start_period_m15 : ", unpack("i", f.read(4))[0])
    print("start_period_m30 : ", unpack("i", f.read(4))[0])
    print("start_period_h1 : ", unpack("i", f.read(4))[0])
    print("start_period_h4 : ", unpack("i", f.read(4))[0])
    print("set_from : ", unpack("i", f.read(4))[0])
    print("freeze_level : ", unpack("i", f.read(4))[0])
    print("generating_errors : ", unpack("i", f.read(4))[0])
    print("generating_errors : ", unpack("i", f.read(4))[0])
    unpack("60i", f.read(240))#paddings

    print("")
    print("---------------------------- TestHistory ----------------------------")
    
    for i in range(1000):
        holc(f)

